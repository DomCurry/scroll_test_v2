﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Thumbnail:MonoBehaviour {

	private ThumbnailVO _thumbnailVO;

//	part of bad solution
//	private bool _enabled;
//	private Image _image;   //keep the reference here, so we don't have to search for it when disabling them
//	private Button _button;

	public ThumbnailVO thumbnailVO {
		get {
			return _thumbnailVO;
		}
		set {
			_thumbnailVO = value;
			UpdateData(_thumbnailVO.id);
		}
	}

	public void UpdateData(string newId) {
		//set the components we want to enable and disable at runtime
		_thumbnailVO.id = newId;
		GetComponent<Image>().sprite = SpriteManager.instance.GetSprite(_thumbnailVO.id);
		GetComponentInChildren<Text>().text = _thumbnailVO.id.ToString();

	}

	public void OnClick() {
		Debug.Log("Thumbnail clicked: "+_thumbnailVO.id);
	}

//	//This solution slows the game down rather than speeds it up
//    //because setting the gameobject to inactive screws with the grid layout, only keep the transform enabled
//    public void disable() {
//        if (!_enabled)
//            return;
//
//        _image.enabled = false;
//        _button.enabled = false;
//
//        //disable the children
//        for (int i = 0; i < transform.childCount; i++) {
//            transform.GetChild(i).gameObject.SetActive(false);
//        }
//		_enabled = false;
//    }
//  
//    public void enable(){
//        if (_enabled)
//            return;
//
//        _image.enabled = true;
//        _button.enabled = true;
//
//        //enable the children
//        for (int i = 0; i < transform.childCount; i++){
//            transform.GetChild(i).gameObject.SetActive(true);
//        }
//		_enabled = true;
//    }
//	//end of bad solution
}