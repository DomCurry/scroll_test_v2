﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThumbnailManager:MonoBehaviour {

	public Transform container;
	public GameObject prefab;

	private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();
	private List<GameObject> _thumbnailList = new List<GameObject>();

	private int _thumbnailsVisible	= 50;
	private int _topThumbnailId		= 0;
	private int _totalThumbnails	= 1000;
	private float _worldRowHeight	= 1.07f;    //I would like to figure out an actual number for this rather than just guessing
	private int _spacing			= 44;


	void Start () {
		createThumbnailVOList();
		createThumbnailPrefabs();
    }
	
	private void createThumbnailVOList() {
		ThumbnailVO thumbnailVO;
		for (int i=_topThumbnailId; i< _thumbnailsVisible; i++) {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }
	}

	private void createThumbnailPrefabs() {
		GameObject gameObj = null;
		for (int i = 0; i < _thumbnailVOList.Count; i++) {
			gameObj = (GameObject)Instantiate(prefab);
			gameObj.transform.SetParent(container, false);
			gameObj.GetComponent<Thumbnail>().thumbnailVO = _thumbnailVOList[i];
            _thumbnailList.Add(gameObj);
        }
	}

	void Update() {
		//go through the game objects, set ones outside the viewport to invisible

		//get the view rectangle
		Rect viewRect = GetComponent<RectTransform>().rect;
		Rect thumbRect;

		bool jumpUp = false;
		bool jumpDown = false;
		for (int i = 0; i < _thumbnailList.Count; i++) {

			thumbRect = GetRelativeRect(_thumbnailList[i]);

			//see if the game object is visible
			if (thumbRect.yMin - _spacing > viewRect.yMax) {
				if (!ToBottom(_thumbnailList[i]))
					continue;
				
				//we're jumping the items in the container, so we sould counter jump the container
				jumpUp = true;
			}
			else if (thumbRect.yMax + _spacing < viewRect.yMin) {
				if (!ToTop(_thumbnailList[i]))
					continue;

				//we're jumping the items in the container, so we sould counter jump the container
				jumpDown = true;
			}
		}

		if (jumpDown) {
			container.transform.position = new Vector3(container.transform.position.x, container.transform.position.y + _worldRowHeight);
		}
		if (jumpUp) {
			container.transform.position = new Vector3(container.transform.position.x, container.transform.position.y - _worldRowHeight);
		}
	}

    Rect GetRelativeRect(GameObject gameObjectWithRectTransform){
        //get the game object's dimensions in this reference frame

        //get the variables we'll use
        Rect rect = gameObjectWithRectTransform.GetComponent<RectTransform>().rect;
        Transform inTransform = gameObjectWithRectTransform.transform;

        //generate the matrix
        Matrix4x4 matrix = SwitchReferenceFrame(inTransform, transform);

        //
        Vector2 bottomLeft     = new Vector2(rect.xMin, rect.yMin);
        Vector2 topRight  = new Vector2(rect.xMax, rect.yMax);

        bottomLeft  = matrix.MultiplyPoint(bottomLeft);
        topRight    = matrix.MultiplyPoint(topRight);

        rect.xMin = bottomLeft.x;
        rect.yMin = bottomLeft.y;
        rect.xMax = topRight.x;
        rect.yMax = topRight.y;

        return rect;
    }

    //transfer coordinate systems between objects.
    Matrix4x4 SwitchReferenceFrame(Transform from, Transform to){
        return to.worldToLocalMatrix * from.localToWorldMatrix;
    }

	//move a thumbnail from the top to the bottom
	bool ToBottom(GameObject thumbnail) {
		//don't do anything if this would move over the total number of thumbnails
		if (_topThumbnailId == _totalThumbnails - 1)
			return false;

		_topThumbnailId++;
		string newId = "" + (_topThumbnailId + _thumbnailsVisible);

		//this does maintain order
		//move the thumbnail to the bottom of the grid
		thumbnail.transform.SetAsLastSibling();

		//give the thumbnail the new data
		thumbnail.GetComponent<Thumbnail>().UpdateData(newId);
		return true;
	}

	//move a thumbnail from the bottom to the top
	bool ToTop(GameObject thumbnail) {
		//don't do anything if this would move below 0
		if (_topThumbnailId == 0)
			return false;
		_topThumbnailId--;

		//maintain order
		string newId = "" + _topThumbnailId;
		
		//move the thumbnail to the top of the grid
		thumbnail.transform.SetAsFirstSibling();

		//give the thumbnail the new data
		thumbnail.GetComponent<Thumbnail>().UpdateData(newId);
		return true;
	}
}
